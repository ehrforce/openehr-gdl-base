import { expect } from "chai";
import "mocha";
import { GuidelineDefinition, Guideline } from "../src/cds/guideline/Guideline";
import {
  Term,
  TermBinding,
  Terminology
} from "../src/cds/terminology/Terminology";
import { TerminologyUtil } from "../src/cds/terminology/TerminologyUtil";
import { ResourceDescription } from "../src/common/resource/ResourceDescription";
import { ResourceDescriptionItem } from "../src/common/resource/ResourceDescriptionItem";

describe("Test go generate a guide", () => {
  const g = generateGuide();

  it("Should be valid", () => {
    expect(g.concept).to.equal("gt0001");
  });
});
/**
 * Generates a simple guide for testing purpose
 */
function generateGuide(): Guideline {
  const def = new GuidelineDefinition();

  const termDefs: Record<string, Record<string, Term>> = {
    nb: {
      gt0001: new Term("gt0001", "ChadVas", "En beskrivelse")
    },
    en: {
      gt0001: new Term("gt0001", "ChadVas", "Some descrtiption")
    }
  };
  const termBind: Record<string, TermBinding> = {
    nb: new TermBinding("gt0001")
  };
  const t = new Terminology(false, "nb", "gt0001", termDefs, termBind);
  const g = new Guideline(
    "CHA2DS2-VASc.v1",
    "gt0001",
    def,
    t,
    TerminologyUtil.GET_LANGUAGE("nb")
  );
  g.description = new ResourceDescription(
    {
      name: "Bjørn Næss",
      email: "bna@dips.no"
    },
    {
      nb: new ResourceDescriptionItem(
        TerminologyUtil.GET_LANGUAGE("nb"),
        "Formålet med denne guiden er å teste litt "
      ),
      en: new ResourceDescriptionItem(
        TerminologyUtil.GET_LANGUAGE("en"),
        "The purpose of this guide is to test javascript"
      )
    }
  );
  return g;
}
