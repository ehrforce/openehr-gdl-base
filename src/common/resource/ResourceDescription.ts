import { AuthoredResource } from "./AuthoredResource";
import { ResourceDescriptionItem } from "./ResourceDescriptionItem";
import { TerminologyCode } from "../../cds/foundation/TerminologyCode";
export class ResourceDescription {
  /**
   * Original author of this resource, with all relevant details, including organisation.
   */
  public original_author: Record<string, string>;
  /**
   * Namespace of original author’s organisation, in reverse internet form, if applicable.
   */
  public original_namespace?: string;
  /**
   * Plain text name of organisation that originally published this artefact, if any.
   */
  public original_publisher?: string;
  public other_contributors?: string[];
  /**
   * initial, submitted, experimental, awaiting_approval, approved, superseded, obsolete.
   */
  public lifecycle_state: TerminologyCode = TerminologyCode.parse(
    "openehr::initial"
  );
  public parent_resource?: AuthoredResource;
  /**
   * Namespace in reverse internet id form, of current custodian organisation.
   */
  public custodian_namespace?: string;
  /**
   * Plain text name of current custodian organisation.
   */
  public custodian_organisation?: string;
  public copyright?: string;
  public licence?: string;
  /**
   * List of acknowledgements of other IP directly referenced in this archetype, typically terminology codes, ontology ids etc. Recommended keys are the widely known name or namespace for the IP source, as shown in the following example:
   * ip_acknowledgements = <
   *     ["loinc"] = <"This content from LOINC® is copyright © 1995 Regenstrief Institute, Inc. and the LOINC Committee, and available at no cost under the license at http://loinc.org/terms-of-use">
   *     ["snomedct"] = <"Content from SNOMED CT® is copyright © 2007 IHTSDO <ihtsdo.org>">
   * >
   */
  public ip_acknowledgements?: Record<string, string>;
  /**
   * List of references of material on which this artefact is based, as a keyed list of strings. The keys should be in a standard citation format.
   */
  public references?: Record<string, string>;
  /**
   * URI of package to which this resource belongs.
   */
  public resource_package_uri?: string;
  /**
   *      * Details related to conversion process that generated this model from an original, if relevant, as a list of name/value pairs. Typical example with recommended tags:
   *  conversion_details = <
   *     ["source_model"] = <"CEM model xyz <http://location.in.clinicalelementmodels.com>">
   *     ["tool"] = <"cem2adl v6.3.0">
   *     ["time"] = <"2014-11-03T09:05:00">
   * >
   *
   *
   */
  public conversion_details?: Record<string, string>;
  /**
   * Additional non-language-sensitive resource meta-data, as a list of name/value pairs.
   */
  public other_details?: Record<string, string>;

  /**
   * Details of all parts of resource description that are natural language-dependent, keyed by language code.
   */
  public details?: Record<string, ResourceDescriptionItem>;
  constructor(
    original_author: Record<string, string>,
    details?: Record<string, ResourceDescriptionItem>
  ) {
    this.original_author = original_author;
    this.details = details;
  }

  public addDetailForLanguage(
    languageCode: string,
    item: ResourceDescriptionItem
  ) {
    if (this.details) {
      this.details.languageCode = item;
    } else {
      this.details = {
        languageCode: item
      };
    }
  }
}
