import {RevisionHistoryItem} from "./RevisionHistoryItem";

/**
 * Purpose Defines the notion of a revision history of audit items, each associated with the version for which that audit was committed. The list is in most-recent-first order.
 */
export class RevisionHistory {
         /**
          *
          * @param items The items in this history in most-recent-last order.
          */
         constructor(public items: RevisionHistoryItem[]) {}
       }


