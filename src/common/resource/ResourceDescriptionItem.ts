import { CodePhrase } from "../../cds/rm/CodePhrase";
import { TerminologyCode } from '../../cds/foundation/TerminologyCode';
/**
 * Language-specific detail of resource description. When a resource is translated for use in another language environment, each RESOURCE_DESCRIPTION_ITEM needs to be copied and translated into the new language.
 */
export class ResourceDescriptionItem {
  /**
   * The localised language in which the items in this description item are written. Coded from openEHR Code Set languages .
   */
  public language: TerminologyCode;
  /**
   * Purpose of the resource.
   */
  public purpose: string;
  /**
     * 	
Keywords which characterise this resource, used e.g. for indexing and searching.
     */
  public keywords?: string[];
  /**
   * Description of the uses of the resource, i.e. contexts in which it could be used.
   */
  public use?: string;
  /**
   * Description of any misuses of the resource, i.e. contexts in which it should not be used.
   */
  public misuse?: string;

  
  /**
   * URIs of original clinical document(s) or description of which resource is a formalisation, in the language of this description item; keyed by meaning.
   */
  public original_resource_uri?: Record<string, string>;

  /**
   * Additional language-senstive resource metadata, as a list of name/value pairs.
   */
  public other_details?: Record<string, string>;

  constructor(language: TerminologyCode, purpose: string) {
    this.language = language;
    this.purpose = purpose;
    }
   
    public addOtherDetail(key: string, value: string) {
        if (this.other_details) {
            this.other_details.key = value;
        } else {
            this.other_details = {
                key: value
            }
        }
    }
    public addKeyword(keyword: string) {
        if (this.keywords) {
            this.keywords.push(keyword);
        } else {
            this.keywords = [keyword];
        }
    }
}
