

import { CodePhrase } from "../../cds/rm/CodePhrase";
import { ResourceDescription } from './ResourceDescription';
import {RevisionHistory} from "./RevisionHistory";
import { TerminologyCode } from '../../cds/foundation/TerminologyCode';
/**
 * Abstract idea of an online resource created by a human author.
 */
export abstract class AuthoredResource {
  constructor(
    public original_language: TerminologyCode,
    public is_controlled: boolean,
    public translations?: AuthoredResourceTranslations,
    public description?: ResourceDescription,
    public revision_history?: RevisionHistory
  ) {}
  public current_version(): string {
    return "1.0";
  }
}
export class TranslationDetails {
  constructor(
    public langauge: CodePhrase,
    public author: Record<string, string>,
    public accreditation?: string,
    public other_details?: Record<string, string>
  ) {}
}

export type AuthoredResourceTranslations = Record<string, TranslationDetails>;
