import {PartyProxy} from "./PartyProxy";
import { DvIdentifier } from '../../cds/rm/DvIdentifier';
/**
 * Proxy data for an identified party other than the subject of the record, minimally consisting of human-readable identifier(s), such as name, formal (and possibly computable) identifiers such as NHS number, and an optional link to external data. There must be at least one of name, identifier or external_ref present.
 * Used to describe parties where only identifiers may be known, and there is no entry at all in the demographic system (or even no demographic system). Typically for health care providers, e.g. name and provider number of an institution.
 * Should not be used to include patient identifying information.
 */
export class PartyIdentified extends PartyProxy {
    public name?: string;
    public identifiers?: DvIdentifier[];
}
