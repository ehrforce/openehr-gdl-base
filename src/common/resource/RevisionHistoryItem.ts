import {AuditDetails} from "./AuditDetails";

/**
 * An entry in a revision history, corresponding to a version from a versioned container. Consists of AUDIT_DETAILS instances with revision identifier of the revision to which the AUDIT_DETAILS intance belongs
 */
export class RevisionHistoryItem {
         /**
          *
          * @param version_id Version identifier for this revision.
          * @param audits The audits for this revision; there will always be at least one commit audit (which may itself be an ATTESTATION), there may also be further attestations.
          */
         constructor(
           public version_id: ObjectVersionId,
           public audits: AuditDetails[]
         ) {}
       }

export class ObjectVersionId{

}
