import {ObjectId} from "./ObjectId";

export class ObjectRef {
         /**
     * 
     * @param namespace Namespace to which this identifier belongs in the local system context (and possibly in any other openEHR compliant environment) e.g. terminology , demographic . 
     * These names are not yet standardised. Legal values for namespace are:
     * "local"
     * "unknown"
     * a string matching the regex [a-zA-Z][a-zA-Z0-9_-:/&+?]*
     * @param type Name of the class (concrete or abstract) of object to which this identifier type refers, e.g. PARTY, PERSON, GUIDELINE etc. 
     * These class names are from the relevant reference model. The type name ANY can be used to indicate that any type is accepted (e.g. if the type is unknown).
     * @param id Globally unique id of an object, regardless of where it is stored.
     */
         constructor(
           public namespace: string,
           public type: string,
           public id: ObjectId
         ) {}
       }


