/**
 * Ancestor class of identifiers of informational objects. Ids may be completely meaningless, in which case their only job is to refer to something, or may carry some information to do with the identified object.
 * Object ids are used inside an object to identify that object. To identify another object in another service, use an OBJECT_REF, or else use a UID for local objects identified by UID. If none of the subtypes is suitable, direct instances of this class may be used.
 */
export abstract class ObjectId {
  constructor(public value: string) {}
}
/**
 * Abstract model of UID-based identifiers consisting of a root part and an optional extension; lexical form: root '::' extension.
 */
export abstract class UidBasedId extends ObjectId {
  constructor(public value: string) {
    super(value);
  }
}
export class HierObjectId extends UidBasedId {
  constructor(public value: string) {
    super(value);
  }
}

/**
 * Globally unique identifier for one version of a versioned object; lexical form: object_id '::' creating_system_id '::' version_tree_id.
 */
export class ObjectVersionId extends UidBasedId {
  constructor(public value: string) {
    super(value);
  }
}

export abstract class UID {
  constructor(public value: string) {}
}
export class ISO_OID extends UID {
  constructor(public value: string) {
    super(value);
  }
}
/**
 * Model of the DCE Universal Unique Identifier or UUID which takes the form of hexadecimal integers separated by hyphens, following the pattern 8-4-4-4-12 as defined by the Open Group, CDE 1.1 Remote Procedure Call specification, Appendix A. Also known as a GUID.
 */
export class UUID extends UID {
  constructor(public value: string) {
    super(value);
  }
}
