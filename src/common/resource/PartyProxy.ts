import {PartyRef} from "./PartyRef";

/**
 * Abstract concept of a proxy description of a party, including an optional link to data for this party in a demographic or other identity management system. Sub- typed into PARTY_IDENTIFIED and PARTY_SELF.
 */
export abstract class PartyProxy {
    public external_ref?: PartyRef;
}

