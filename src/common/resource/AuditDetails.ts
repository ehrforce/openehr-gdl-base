
import { DvCodedText } from '../../cds/rm/DvCodedText';
import { DvText } from '../../cds/rm/DvText';
import { DvDateTime } from '../../cds/rm/DvDateTime';
import {PartyProxy} from './PartyProxy';

/**
 * The set of attributes required to document the committal of an information item to a repository.
 */
export class AuditDetails {
         /**
          * Identifier of the logical EHR system where the change was committed. This is almost always owned by the organisation legally responsible for the EHR, and is distinct from any application, or any hosting infrastructure.
          */
         public system_id: string;

         /**
          * Time of committal of the item.
          */
         public time_committed: DvDateTime;

         /**
          * Type of change. Coded using the openEHR Terminology audit change type group.
          */
         public change_type: DvCodedText;

         /**
          * Reason for committal. This may be used to qualify the value in the change_type field. For example, if the change affects only the EHR directory, this field might be used to indicate 'Folder "episode 2018-02-16" added' or similar.
          */
         public description?: DvText;

         /**
          * Identity and optional reference into identity management service, of user who committed the item.
          */
    public committer: PartyProxy;
    
    constructor(systemId: string, timeCommitted: DvDateTime, changeType: DvCodedText, committer: PartyProxy) {
        this.system_id = systemId;
        this.time_committed = timeCommitted;
        this.change_type = changeType;
        this.committer = committer;
        
    }
}
       

