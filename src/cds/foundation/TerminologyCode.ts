import {Uri} from "./Uri";


/**
 * Logically primitive type representing a reference to a terminology concept, in the form of a terminology identifier, optional version, and a code or code string from the terminology.
 */
export class TerminologyCode {
  /**
   * The archetype environment namespace identifier used to identify a terminology. Typically a value like "snomed_ct" that is mapped elsewhere to the full URI identifying the terminology.
   */
  public terminology_id: string;

  /**
   * Optional string value representing terminology version, typically a date or dotted numeric.
   */
  public terminology_version?: string;

  /**
   * A terminology code or post-coordinated code expression, if supported by the terminology. The code may refer to a single term, a value set consisting of multiple terms, or some other entity representable within the terminology.
   */
  public code_string: string;

  /**
   * The URI reference that may be used as a concrete key into a notional terminology service for queries that can obtain the term text, definition, and other associated elements.
   */
  public uri?: Uri;
  constructor(terminology_id: string, code_string: string) {
    this.terminology_id = terminology_id;
    this.code_string = code_string;
    }
    
    /**
     * 
     * @param value '<terminology_id>::<ccode_string>'
     */
    public static parse(value: string): TerminologyCode{
        const arr = value.split("::");
        return new TerminologyCode(arr[0], arr[1]);
    }
}


