/**
 * 	The binding between a specific node in an archetype and a variable in the guide.
 */
export class Element {
         /**
          *
          * @param id The gt-code of this element.
          * @param path The path within archetype to reach this element.
          */
         constructor(public id: string, public path: string) {}
       }
