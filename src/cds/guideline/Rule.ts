import { Assertion } from '../expression/Assertion';



/**
 * 	A single rule defined in a guideline, of the logical form
 * when conditions then actions.
 * Conditions are assertions based on input and/or environment variables (e.g.time, date etc). Actions take the form of assignments to output variables.
 */
export class Rule {
         /**
          *
          * @param id The gt-code of this rule.
          * @param priority Indicates the evaluation order of Rules within a Guideline. Rules with a higher priority value are executed first.
          * @param when List of assertions that when evaluated to True cause the rule to be fired. The when assertions may only mention 'input' type variables.
          * @param then List of actions if the rule is fired. Each action is in the form of an assignment to an 'output' type variable.
          */
         constructor(
           public id: string,
           public priority: number,
           public when?: Assertion[],
           public then?: Assertion[]
         ) {}
       }
