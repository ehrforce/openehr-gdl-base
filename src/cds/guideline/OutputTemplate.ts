/**
 * Definition of structure for reporting guideline execution results.
 */
export class OutputTemplate {
  /**
   *
   * @param id The gt-code of this output template.
   * @param name Name of this template.
   * @param model_id Identifier of a model that defines the structure of the object.
   * @param object Detailed output report structure as an object that may be defined by a published model.
   * @param template_id The identifier of a template based on the model identified by model_id defining the attached object.
   */
  constructor(
    public id: string,
    public name: string,
    public model_id: string,
    public object: string,
    public template_id?: string
  ) {}
}
