
import { Terminology } from "../terminology/Terminology";
import {Rule} from "./Rule";
import {Assignment} from "../expression/Statement";
import {Variable} from "../expression/Variable";
import {DataBinding} from "./DataBinding";
import {OutputTemplate} from "./OutputTemplate";

import { Assertion } from '../expression/Assertion';
import { AuthoredResource } from '../../common/resource/AuthoredResource';
import { CodePhrase } from '../rm/CodePhrase';
import { TerminologyCode } from '../foundation/TerminologyCode';

export class Guideline extends AuthoredResource {
  /**
   *
   * @param id Identification of this guideline. Syntax is a string of format:
   * concept_name '.v' [N[.M.P]]
   * For example: calculate_bmi.v1.
   * @param concept The normative meaning of the guideline as whole. Expressed as a gt-code.
   * @param definition The main definition part of the guide. It consists of archetype bindings and rule definitions.
   * @param terminology Terminology definitions of the guideline containing definitions and translations (if applicable) for every gt-code
   * @param gdl_version The version of the GDL the guide is written in, in a string of format:
   * N.M[.P]
   * i.e. a 2- or 3-number form of version, e.g. "1.0", "2.0", etc.
   */
  constructor(

    public id: string,
    public concept: string,
    public definition: GuidelineDefinition,
    public terminology: Terminology,
    original_language: TerminologyCode,
    public gdl_version = "2.0"

  ) {
    super(original_language,false);
  }
}
/**
 * The definition of the guideline includes:
 * pre_conditions: conditions defining the applicability of this guideline to a subject;
 * data bindings: bindings to to archetypes and other sources;
 * rules: define the logic of the guideline;
 * template: defines the output reporting structure.
 */
export class GuidelineDefinition {
         /**
          * List of archetype/template bindings, which define specific elements to be used by rules in this guideline.
          */
         public data_bindings?: Record<string, DataBinding>;
         /**
          * Pre-conditions to be met before the guideline can be selected for execution. Pre-conditions constitute the definition of the guideline 'entry point', i.e. the match to a patient type to which the guideline may apply.
          */
         public pre_conditions?: Assertion[];
         /**
          * Map of rules indexed by local gt codes.Map of rules indexed by local gt codes.
          */
         public rules?: Record<string, Rule>;
         /**
          * Actions that will always be performed, whenever the when conditions are evaluated, regardless of the result.
          */
         public default_actions?: Assignment[];
         /**
          * One or more output termplates that define how to represent output reporting structure.
          */
         public templates?: Record<string, OutputTemplate>;
         /**
          * Variables used internally to a GDL guideline, i.e. not bound to external data entities. These are generated during parsing of expressions and used to track interim state.
          */
         public internal_variables?: Variable[];
         constructor() {}
       }


