import { ExpressionItem } from './ExpressionItem';
import { OperatorKind } from './OperatorKind';
export abstract class Operator extends ExpressionItem {
         /**
          *
          * @param operator The operator type of this expression.
          */
         constructor(public operator: OperatorKind) {
           super();
         }
       }