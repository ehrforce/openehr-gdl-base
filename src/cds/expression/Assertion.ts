import {ExpressionItem} from "./ExpressionItem";
import {Statement} from "./Statement";
/**
 * A statement of the logical form assert(cond) that asserts the truth of a Boolean expression, which if not true, generates an exception at runtime.
 */
export class Assertion extends Statement {
  /**
   *
   * @param expression Expression that is the subject of the assertion.
   */
  constructor(public expression: ExpressionItem) {
    super();
  }
}
