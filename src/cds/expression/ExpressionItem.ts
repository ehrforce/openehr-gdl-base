/**
 * 	Abstract parent of all valued elements that may be used as a term in an expression. Concrete descendant types are generated by a GDL2 parser when processing a GDL2 text.
 * 
 * The function type() returns a value for the type of any concrete element, which may have been inferred and stored during parsing (Constants and Variables), or may be looked up (library of function calls).
 */
export abstract class ExpressionItem {
         /**
          * Type name of the item. Comparisons should be done case-insensitive.
          */
         public abstract type(): string;
       }
