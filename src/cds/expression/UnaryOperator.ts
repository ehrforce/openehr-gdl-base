import { Operator } from './Operator';
import { OperatorKind } from './OperatorKind';
import { ExpressionItem } from './ExpressionItem';
export class UnaryOperator extends Operator{
    public type(): string {
        throw new Error("Method not implemented.");
    }

    constructor(public operator: OperatorKind, public operand: ExpressionItem) {
        super(operator);
    }

}

/**
 * Concrete model of a binary operator in the rule.
 */
export class BinaryOperator extends Operator{

    constructor(public operator: OperatorKind, public left: ExpressionItem, public right: ExpressionItem) {
        super(operator);
    }
    public type(): string {
        throw new Error("Method not implemented.");
    }
    
}