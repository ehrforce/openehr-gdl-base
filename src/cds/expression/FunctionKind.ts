export enum FunctionKind {
  /**Returns the absolute value of a double value. */
  abs,
  /**Returns the smallest double value that is greater than or equal to the argument and is equal to a mathematical integer. */
  ceil,

  /**Returns Euler’s number e raised to the power of a double value. */
  exp,

  /**Returns the largest double value that is less than or equal to the argument and is equal to a mathematical integer. */
  floor,
  /**Returns the natural logarithm (base e) of a double value. */
  log,

  /**Returns the base 10 logarithm of a double value. */
  log10,

  /**Returns the natural logarithm of the sum of the argument and 1. */
  log1p,
  /**Returns the closest long to the argument, with ties rounding to positive infinity. */
  round,

  /**Returns the correctly rounded positive square root of a double value. */
  sqrt,

  /**Returns the trigonometric sine of an angle. */
  sin,

  /**Returns the trigonometric cosine of an angle. */

  cos,

  /**Maximum value of N elements. */
  max,

  /**Minimum value of N elements. */
  min,

  /**Count of instances of an element. */

  count,

  /**Total sum of N element values. */
  sum
}
