import { Variable } from "./Variable";
import { ExpressionItem } from "./ExpressionItem";
export abstract class Statement {}
/**
 * A statement assigning an expression result to a variable.
 */
export class Assignment extends Statement {
  /**
   *
   * @param variable Variable that is the target of the assignment.
   * @param expression Expression that is the source of the assignment.
   */
  constructor(public variable: Variable, public expression: ExpressionItem) {
    super();
  }
}


