import { ExpressionItem } from "./ExpressionItem";
/**
 * Terminal expression element representing a named variable with a definition provided by the code attribute, containing a gt-code.
 */
export class Variable extends ExpressionItem {
         /**
          *
          * @param name Variable symbolic name.
          * @param code Internal gt-code defining the meaning of this variable.
          */
         constructor(public name: string, public code: string) {
           super();
         }
         public type(): string {
           throw new Error("Method not implemented.");
         }
       }
