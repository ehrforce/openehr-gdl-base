/**
 * 
 */
export enum OperatorKind {
  /**Addition ('+') operation. */
  Addition = "+",
  Subtraction = "-",
  Multiplication = "*",
  Division = "/",
  /**Exponent ('^') operation. */
  Exponent = "^",
  And = "&&",
  Or = "||",
  Not = "!",
  Equal = "==",
  Unequal = "!=",
  LessThan = "<",
  LessThanOrEqual = "<=",
  GreaterThan = ">",
  GreaterThanOrEqual = ">=",
  Assignment = "=",
  Is_a = "is_a",
  Is_not_a = "!is_a",
  /**Universal quantifier operator ('∀'). */
  For_all = "∀"
}
