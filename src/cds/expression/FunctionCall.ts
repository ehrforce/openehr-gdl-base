import { ExpressionItem } from './ExpressionItem';
import { FunctionKind } from './FunctionKind';
export class FunctionCall extends ExpressionItem{

    constructor(public func: FunctionKind, public items: ExpressionItem[] | undefined) {
        super();
    }
    public type(): string {
        throw new Error("Method not implemented.");
    }


}