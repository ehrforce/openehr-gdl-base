import { ExpressionItem } from "./ExpressionItem";

import {Iso8601DateTime} from "../rm/Iso8601DateTime";
import {DvCodedText} from "../rm/DvCodedText";
import {DvOrdinal} from "../rm/DvOrdinal";
import {DvQuantity} from "../rm/DvQuantity";
import { CodePhrase } from '../rm/CodePhrase';
export abstract class Constant extends ExpressionItem {
  constructor(public name: string, public value: any) {
    super();
  }
  public type(): string {
    throw new Error("Method not implemented.");
  }
}
export class StringConstant extends Constant {
  constructor(public name: string, public value: string) {
    super(name, value);
  }
}

export class IntegerConstant extends Constant {
  constructor(public name: string, public value: number) {
    super(name, value);
  }
}

export class DateTimeConstant extends Constant{
    constructor(public name: string, public value: Iso8601DateTime) {
        super(name, value);
    }
}
export class CodePhraseConstant extends Constant{
    constructor(public name: string, public value: CodePhrase) {
        super(name, value);
    }

}
export class CodedTextConstant extends Constant{
    constructor(public name: string, public value: DvCodedText) {
        super(name, value);
    }
}
export class OrdinalConstant extends Constant{
    constructor(public name: string, public value: DvOrdinal) {
        super(name, value);
    }
}
export class QuantityConstant extends Constant{
    constructor(public name: string, public value: DvQuantity) {
        super(name, value);    
    }
}
