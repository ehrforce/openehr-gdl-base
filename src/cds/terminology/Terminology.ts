/**
 * Local terminology of an archetype. This class defines the semantics of the terminology of an archetype.
 */
export class Terminology {
  /**
   *
   * @param is_differential Indicates whether this terminology is differential with respect to a specialisation parent, or complete.
   * @param original_language Original language of the terminology, as set at creation or parsing time; must be a code in the ISO 639-1 2 character language code-set.
   * @param concept_code Term code defining the meaning of the artefact as a whole, and always used as the at-code on the root node of the archetype. Must be defined in the term_definitions property.
   * @param term_definitions Directory of term definitions as a two-level table. The outer hash keys are language codes, e.g. "en", "de", while the inner hash keys are term codes, e.g. "gt17".
   * @param term_bindings Directory of bindings to external terminology codes and value sets, as a two-level table. The outer hash keys are terminology ids, e.g. "SNOMED_CT", and the inner hash keys are constraint codes, e.g. "at4", "ac13" or paths. The indexed DV_URI objects represent references to externally defined resources, either terms, ontology concepts, or terminology subsets / ref-sets.
   */
  constructor(
    public is_differential: boolean,
    public original_language: string,
    public concept_code: string,
    public term_definitions: Record<string, Record<string, Term>>,
    public term_bindings: Record<string, TermBinding>
  ) {}
  /**
   * Specialisation depth of this artefact.
   * Unspecialised artefacts have depth 0, with each additional level of specialisation adding 1 to the specialisation_depth.
   */
  public specialisation_depth(): number {
    return 0;
  }
  /**
   *
   * @param a_lang True if language a_lang is present in archetype terminology.
   */
  public has_language(a_lang: string): boolean {
    return false;
  }
  /**
   *
   * @param a_code True if code a_code defined in this terminology.
   */
  public has_term_code(a_code: string): boolean {
    return true;
  }
  /**
   *
   * @param a_lang Term definition for a code, in a specified language.
   * @param a_code
   */
  public term_definition(a_lang: string, a_code: string): Term {
    return new Term("code", "text", "description");
  }

  /**
   * Binding of constraint corresponding to a_code in target external terminology a_terminology_id, as a string, which is usually a formal query expression.
   * @param a_terminology
   * @param a_code
   */
  public term_binding(a_terminology: string, a_code: string): Uri {
    return new Uri();
  }
  /**
   * List of terminologies to which term or constraint bindings exist in this terminology, computed from bindings.
   */
  public terminologies_available(): string[] {
    return [];
  }
  /**
   * List of languages in which terms in this terminology are available.
   */
  public languages_available(): string[] {
    return [];
  }
}
export class Uri {}
export class Term {
  /**
   *
   * @param code Code of this term.
   * @param text Short term text, typically for display.
   * @param description Full description text.
   * @param other_items Hash of keys and corresponding values for other items in a term, e.g. provenance. Hash of keys ("text", "description" etc) and corresponding values.
   */
  constructor(
    public code: string,
    public text: string,
    public description: string,
    public other_items?: Record<string, string>
  ) {}
}
/**
 * One binding between a local gt-code and one or more external bindings
 */
export class TermBinding {
    constructor(public code: string, public bindings?: Term[]){}
    
    
}
