import { TerminologyCode } from '../foundation/TerminologyCode';

export class TerminologyUtil{

    public static GET_LANGUAGE(lang: string): TerminologyCode {
        return TerminologyCode.parse(`openehr::${lang}`);
        
    }

}